FROM python:3

ENV PYTHONUNBUFFERED=1
ENV PYTHONDONTWRITEBYTECODE=1

WORKDIR /django_blog
COPY ./requirements.txt .

RUN pip install -r requirements.txt
